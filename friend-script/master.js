/********************************************************
        
        Mass Friendship - Master Script

            Author: Gabriel VR
            Date: 29/07/2018
        

********************************************************/

WORLD = 58;
LOCALE = 'pt';

//  ================================================
// 
//      Setup
// 

$c = t => document.createElement(t);

iframe = $c('iframe');
wrap = $c('div');

$q = q => iframe.contentDocument.querySelector(q);
$qa = q => iframe.contentDocument.querySelectorAll(q);

cursor = 0;

dataUsers = $c('textarea');
dataPass = $c('textarea');
startBtn = $c('button');
table = $c('table');
led = $c('div');

function init() {

    document.body.remove();
    document.body = $c('body');

    wrap.classList.add('wrap');
    document.body.appendChild(wrap);

    createIframe();

    function _css() {
        let style = $c('style');

        style.innerText = `
            body {
                overflow: hidden;
            }
            .wrap {
                background: rgba(0,0,0, .6);
                position: fixed;
                top: 20px;
                left: 0; right: 0;
                margin: auto;
                width: 800px;
                min-height: 200px;
            }
            iframe {
                border: none;
                width: 100%;
                height: 100vh;
            }
            .gscript-table {
                border-collapse: collapse;
                color: #fff;
                margin: 8px auto;
            }
            .gscript-table th, .gscript-table td {
                border: solid thin #fff;
                padding: 4px 8px;
            }
            textarea {
                min-width: 160px;
                min-height: 300px;
            }
            .start-btn {
                display: block;
                font-size: 18px;
                padding: 4px 12px;
                margin: 8px auto;
            }
            .led {
                width: 30px;
                height: 30px;
                border-radius: 50%;
                background-color: #ccc;
                margin: 8px 20px;
            }
        `;

        document.head.appendChild(style);
    }
    _css();

    function _createUi() {
        // Indicator
        led.classList.add('led');
        wrap.appendChild(led);

        // Btn
        startBtn.innerText = 'Iniciar';
        startBtn.classList.add('start-btn');
        wrap.appendChild(startBtn)

        // Table

        table.classList.add('gscript-table')

        let group, row, cell;

        group = $c('thead');
        row = $c('tr');
        cell = $c('th');
        cell.innerText = 'Login';

        table.appendChild(group);
        group.appendChild(row);
        row.appendChild(cell);

        cell = $c('th');
        cell.innerText = 'Senha';
        row.appendChild(cell);

        group = $c('tbody');
        row = $c('tr');
        table.appendChild(group);
        group.appendChild(row);

        cell = $c('td');
        cell.appendChild(dataUsers);
        row.appendChild(cell);

        cell = $c('td');
        cell.appendChild(dataPass);
        row.appendChild(cell);

        dataUsers.cols = '25';
        dataUsers.rows = '24';
        dataPass.rows = '24';

        wrap.appendChild(table)
    }
    _createUi();

    startBtn.onclick = () => start();

}


//  ================================================
// 
//      Main
// 

function start() {
    users = dataUsers.value.split('\n').filter(i => i);
    passs = dataPass.value.split('\n').filter(i => i);

    // users = ['mouse.', 'estrela21', 'amanda flores'];;
    // passs = ['145145', '2121', '3535'];


    if (!users.length || !users[0] || !passs.length || !passs[0] ||
        users.length !== passs.length
    ) {
        console.warn('As contas não estão corretas.')
        return;
    }

    if (users.length < 2) {
        console.warn('É preciso no minímo 2 contas para executar o script.')
        return;
    }

    led.style.background = '#4af';

    startBtn.style.display = 'none';
    table.style.display = 'none';

    login(users[0], passs[0], () => {
        alert('Adicione o segundo script na janela que irá abrir.');
        conn();
        check();
    })

}



//  ================================================
// 
//      Fns
// 

function createIframe(cb) {
    iframe = $c('iframe');

    document.body.prepend(iframe);

    iframe.src = location;

    iframe.onload = () => {
        iframe.onload = null;
        if (cb) setTimeout(() => cb(), 1e3);
    }
}

function login(u, p, cb) {
    if ($q('#login_form')) {
        s1();
    } else if ($q('.worlds-container')) {
        iframe.onload = () => s1();
        iframe.contentWindow.location = '/page/logout';
    } else {
        console.warn('Não foi possivel logar conta');
    }


    function s1() {
        $q('#user').value = u;
        $q('#password').value = p;
        $q('#remember-me').checked = false

        iframe.onload = () => s2();

        $q('.btn-login').click();
        $q('#user').value = u;
        $q('#password').value = p;
        $q('#remember-me').checked = false

        iframe.onload = () => s2();

        $q('.btn-login').click();
    }


    function s2() {
        console.log('Loged in');

        const nodes = $qa('.worlds-container span');
        let w;
        const re = new RegExp(` ${WORLD}$`);

        for (const i of nodes)
            if (re.test(i.innerText)) {
                w = i;
                break;
            }

        if (!w) {
            console.log('Mundo não encontrado');
            return;
        }

        iframe.onload = () => s3();
        w.click();
    }

    function s3() {
        console.log('World selected')

        try {
            const join = $q('#join');
            if (!join) {
                s4();
                return;
            }

            iframe.onload = () => s4();
            $q('.btn').click();
        } catch (ex) {
            s4();
        }


    }

    function s4() {
        iframe.onload = null;
        iframe.remove();
        createIframe(() => {
            if (cb) setTimeout(() => cb(), 1e3);
        });


    }
}


function firstInvite() {
    console.log(`Convidando ${users[1]}...`);

    send(`invite:${users[1]}`);

}

function invite() {

    if (users[cursor + 1]) {
        login(users[cursor + 1], passs[cursor + 1], () => {
            login(users[cursor], passs[cursor], () => {
                console.log(`Convidando ${users[cursor + 1]}...`);

                send(`invite:${users[cursor + 1]}`);
            })
        })
    } else {
        login(users[cursor], passs[cursor], () => {
            console.log(`Aceitando ${users[cursor - 1]}...`);

            send(`accept:${users[cursor - 1]}`);
        })
    }



}

//  ================================================
// 
//      Comunicação
// 

connected = false;

function conn() {
    led.style.background = '#fa5';

    $slave = window.open(`https://${LOCALE}${WORLD}.tribalwars.com.${LOCALE}/game.php?screen=buddies`);

    window.addEventListener("message", receiveMessage, false);

    function receiveMessage(e) {

        if (!/^gscript-/.test(e.data)) {
            console.log(e.data);
            return;
        }

        const c = e.data.split('gscript-')[1];

        if (c === 'slave-opened') {
            connected = true;
            led.style.background = '#4f9';
            console.log('Escravo conectado.');
            return;
        }

        handle(c);
    }
}

function send(msg) {
    if (!$slave) return;

    $slave.postMessage('gscript-' + msg, `https://${LOCALE}${WORLD}.tribalwars.com.${LOCALE}`)
}

function handle(c) {
    if (c === 'invitation-sent') {
        if (cursor === 0) {
            cursor = 1;
            invite();
            return;
        }

        if (cursor > 0) {
            console.log(`aceitando ${users[cursor - 1]}`);
            send(`accept:${users[cursor - 1]}`);
        }


        return;
    }

    if (c === 'realocated-done') {
        if (users[cursor + 1]) {
            cursor++;
            invite();
        } else {
            console.log('Finish!');
        }

        console.log('Realocado, próximo...');

        return;
    }

    console.log(c);

}


function check() {

    function _check() {
        if (connected) {
            invite();
            return;
        }

        send('hello?');

        setTimeout(() => _check(), 3e3);
    }

    _check();

}


clear();
init();