/********************************************************
        
        Mass Friendship - Slave Script

            Author: Gabriel VR
            Date: 29/07/2018
        

********************************************************/

WORLD = 58;
LOCALE = 'pt';

//  ================================================
// 
//      Setup
// 

$c = t => document.createElement(t);
iframe = $c('iframe');
$q = q => iframe.contentDocument.querySelector(q);
$qa = q => iframe.contentDocument.querySelectorAll(q);
$w = () => iframe.contentWindow;

function init() {

    document.body.remove();
    document.body = $c('body');
    document.body.appendChild(iframe);

    iframe.style.border = 'none';
    iframe.style.width = '100%';
    iframe.style.height = '100vh';

    iframe.onload = () => conn();
    iframe.src = location;
}



//  ================================================
// 
//      Fns
// 

function inviteFriend(name) {
    iframe.onload = () => s1();
    $w().location = 'game.php?screen=buddies';

    function s1() {
        const i = $qa('input');

        i[0].value = name;

        iframe.onload = () => s2();
        i[1].click();

    }

    function s2() {
        iframe.onload = null;

        const err = $q('.error_box');

        if (err) {
            send(`error:${err.innerText}`);
            return;
        }

        console.log('Pedido Enviado!');
        send('invitation-sent');
    }

}

function acceptFriend(name) {

    if (/^\??(.*&)?screen=buddies&?$/.test($w().location.search)) {
        s1();
    } else {
        iframe.onload = () => s1();
        $w().location = 'game.php?screen=buddies';
    }

    function s1() {
        const h = Array.prototype.find.call($qa('h3'), i => i.innerText === 'Convites em aberto');

        if (!h) {
            console.warn('Não há convites.');
            return;
        }


        const nodes = h.nextElementSibling.querySelectorAll('a');

        let node = Array.prototype.find.call(nodes, i => i && i.innerText === name);
        if (node) {
            node = node.parentElement.parentElement.querySelector('a.btn-confirm-yes');
        }
        if (!node) {
            console.warn('Convite não encontrado.');
            return;
        }

        iframe.onload = () => s2();
        node.click();
    }

    function s2() {
        iframe.onload = () => s3();
        $w().location = 'game.php?screen=inventory';
    }

    function s3() {
        const src = `https://ds${LOCALE}.innogamescdn.com/8.135/37857/graphic/items/relocate.png`;

        let node;

        let tries = 0;

        function _findItem() {
            node = Array.prototype.find.call($qa('img'), i => i && i.src === src);

            if (!node) {
                if (tries > 3) {
                    console.warn('Item não encontrado.');
                    return;
                }
                tries++;
                setTimeout(() => _findItem(), 3e3);
                return;
            }

            node.click();
            _findBtn();

        }

        _findItem();

        
        function _findBtn() {
            const btn = $q('.btn');

            if (btn) {
                btn.click();
                s4();
                return;
            }

            setTimeout(() => _findBtn(), 3e3);
        }

    }

    function s4() {

        function _find() {
            const btn = $q('#village_relocation_settings .btn-default');

            s = $q('#village_relocation_settings select');

            if (btn) {
                btn.click();
                s5();
                return;
            }

            setTimeout(() => _find(), 3e3);
        }

        _find();

    }

    function s5() {

        function _find() {
            const btn = $q('.evt-confirm-btn');

            if (btn) {
                iframe.onload = () => s6();
                btn.click();
                return;
            }

            setTimeout(() => _find(), 3e3);
        }

        _find();

    }

    function s6() {
        console.log('Movido!');
        iframe.onload = null;

        send('realocated-done');
    }


}


//  ================================================
// 
//      Comunicação
// 


function conn() {
    iframe.onload = null;

    window.addEventListener("message", receiveMessage, false);

    function receiveMessage(e) {

        if (!/^gscript-/.test(e.data)) {
            console.log(e.data);
            return;
        }

        const c = e.data.split('gscript-')[1];

        if (c === 'hello?') {
            $master = e.source;
            send('slave-opened');
            console.log('Conexão com mestre estabelecida.');
            return;
        }

        handle(c);

    }
}

function send(msg) {
    $master.postMessage('gscript-' + msg, `https://www.tribalwars.com.${LOCALE}`)
}

function handle(c) {

    if (/^invite:/.test(c)) {
        const n = c.split('invite:')[1];
        if (n) {
            inviteFriend(n);
        }
        return;
    }

    if (/^accept:/.test(c)) {
        const n = c.split('accept:')[1];
        if (n) {
            acceptFriend(n);
        }
        return;
    }


    console.log(c);
}



clear();
init();